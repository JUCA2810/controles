package com.example.myapplicationprcatica_3.wi

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import java.text.DateFormat
import java.util.*

class TimePickerfragment: DialogFragment() {

    private var listener : TimePickerDialog.OnTimeSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        return TimePickerDialog(activity, listener, hour, minute, android.text.format.DateFormat.is24HourFormat(activity))
    }

    companion object {
        fun newInstace(listener: TimePickerDialog.OnTimeSetListener) :TimePickerfragment {
            val fragment = TimePickerfragment()
            fragment.listener = listener
            return fragment
        }
    }

    //override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
    //    Toast.makeText(activity, "${hourOfDay}:${minute}", Toast.LENGTH_LONG).show()
    //}

}